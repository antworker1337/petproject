FROM node:17-alpine
EXPOSE 3000
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci
COPY . .
CMD node app.js
