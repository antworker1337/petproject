const express = require('express');
const bodyParses = require('body-parser');
const handlebars = require('express-handlebars');
const path = require('path');
const logger = require('./libs/log');
const db = require('./models/db');
require('dotenv').config()

const host = 'localhost'
const port = 3000
const app = express()

app.use(bodyParses.json());

app.use(bodyParses.urlencoded({ extended: true }));

console.log(process.env)

app.engine(
    'handlebars',
    handlebars.engine({ defaultLayout: 'main' })
)
app.set('views', path.join(__dirname + '/views'))
app.set('view engine', 'handlebars')

app.get('/', (req, res) => {
    res.render('home', { title: 'My little Handlebars homepage' })
})

app.get('/users', db.getAllUsers)
app.get('/users/:id', db.getUser)
app.post('/users', db.createUser)
app.put('/users/:id', db.updateUser)
app.delete('/users/:id', db.deleteUser)

app.listen(port, host, function () {
    logger.info(`Server is listening on ${ host }:${ port }`)
})
