const Pool = require('pg').Pool;

const pool = new Pool({
    user: 'fl0k13',
    host: 'localhost',
    database: 'common',
    password: '1',
    port: 5432
})

const getAllUsers = (req, res) => {
    pool.query('SELECT * FROM users ORDER BY id ASC', (error, result) => {
        if (error) {
            throw new Error("Sorry but no")
        }
        res.status(200).json(result.rows)
    })
}

const getUser = (req, res) => {
    const id = parseInt(req.params.id)

    pool.query('SELECT * FROM users WHERE id = $1', [id], (error, result) => {
        if (error) {
            throw new Error("Nah not")
        }
        res.status(200).json(result.rows)
    })
}

const createUser = (req, res) => {
    const { name, email } = req.body

    pool.query('INSERT INTO users (name, email) VALUES ($1, $2) RETURNING *', [name, email], (error, result) => {
        if (error) {
            throw new Error("Hell no")
        }
        res.status(201).send(`User successfully created with id:${ result.rows[0].id }`)
    })
}

const updateUser = (req, res) => {
    const id = parseInt(req.params.id)
    const { name, email } = req.body

    pool.query(
        'UPDATE users SET name = $1, email = $2 WHERE id = $3',
        [name, email, id],
        (error, results) => {
            if (error) {
                throw error
            }
            res.status(200).send(`User modified with ID: ${id}`)
        }
    )
}

const deleteUser = (req, res) => {
    const id = parseInt(req.params.id)

    pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
        if (error) {
            throw error
        }
        res.status(200).send(`User deleted with ID: ${id}`)
    })
}



module.exports = {
    getAllUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser
}
