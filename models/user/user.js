module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
        id: {
            type: Sequelize.INT,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING_TYPE
        },
        email: {
            type: Sequelize.STRING_TYPE
        }
    })

    return User;
}
